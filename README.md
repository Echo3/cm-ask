# Call Me - Alexa Skill Kit 

### Alexa Skills Kit Documentation
The documentation for the Alexa Skills Kit is available on the [Amazon Apps and Services Developer Portal](https://developer.amazon.com/appsandservices/solutions/alexa/alexa-skills-kit/).

### Description
Call me app solves a problem we have all encountered. When you have lost your phone and can't seem to find it within your home. 
Simply ask Alexa to "Call me" and it will call your phone. 

#### Example
- [WiseGuy](samples/wiseGuy) : a skill that tells knock knock jokes.

### Resources
Here are a few direct links to our documentation:

- [Using the Alexa Skills Kit Samples (Node.js)](https://developer.amazon.com/public/solutions/alexa/alexa-skills-kit/docs/using-the-alexa-skills-kit-samples)
- [Getting Started](https://developer.amazon.com/appsandservices/solutions/alexa/alexa-skills-kit/getting-started-guide)
- [Invocation Name Guidelines](https://developer.amazon.com/public/solutions/alexa/alexa-skills-kit/docs/choosing-the-invocation-name-for-an-alexa-skill)
- [Developing an Alexa Skill as an AWS Lambda Function](https://developer.amazon.com/appsandservices/solutions/alexa/alexa-skills-kit/docs/developing-an-alexa-skill-as-a-lambda-function)
- [Setup for ssh keys to commit and pull from repository](https://confluence.atlassian.com/bitbucketserver/creating-ssh-keys-776639788.html)

